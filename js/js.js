//Плагин для тайминг-функций в jQ
(function(factory){if(typeof exports==="object"){factory(require("jquery"))}else if(typeof define==="function"&&define.amd){define(["jquery"],factory)}else{factory(jQuery)}})(function($){$.extend({bez:function(encodedFuncName,coOrdArray){if($.isArray(encodedFuncName)){coOrdArray=encodedFuncName;encodedFuncName="bez_"+coOrdArray.join("_").replace(/\./g,"p")}if(typeof $.easing[encodedFuncName]!=="function"){var polyBez=function(p1,p2){var A=[null,null],B=[null,null],C=[null,null],bezCoOrd=function(t,ax){C[ax]=3*p1[ax],B[ax]=3*(p2[ax]-p1[ax])-C[ax],A[ax]=1-C[ax]-B[ax];return t*(C[ax]+t*(B[ax]+t*A[ax]))},xDeriv=function(t){return C[0]+t*(2*B[0]+3*A[0]*t)},xForT=function(t){var x=t,i=0,z;while(++i<14){z=bezCoOrd(x,0)-t;if(Math.abs(z)<.001)break;x-=z/xDeriv(x)}return x};return function(t){return bezCoOrd(xForT(t),1)}};$.easing[encodedFuncName]=function(x,t,b,c,d){return c*polyBez([coOrdArray[0],coOrdArray[1]],[coOrdArray[2],coOrdArray[3]])(t/d)+b}}return encodedFuncName}})});;
//Код страниц
$(function(){
	try {
		let sliderIndex = $('section.d-slider .d-slider-container').slick({
			slidesToShow: 5,
			centerMode: true,
		    arrows: true,
		    nextArrow: $('.next'),
		    prevArrow: $('.prev'),
		    focusOnSelect: true,
		    adaptiveHeight: true,
		    centerPadding: '0px',
		});
	
		let sliderUser = $('section.work-space .user-slider-container').slick({
			slidesToShow: 3,
			centerMode: true,
		    arrows: true,
		    nextArrow: $('.next'),
		    prevArrow: $('.prev'),
		    focusOnSelect: true,
		    adaptiveHeight: true,
		    centerPadding: '0px',
		});
	} catch(e){
		console.log(e)
	}

// Вопросы скрыть
	
	$('.d-a').hide();	

// Синхронизация шапки таблицы и тела
	$('.table_container').has('.scrollable').on('scroll', function(e){
		this.style.position = 'relative';
		$('thead.scrollable').css({'position': 'absolute'}).offset({
			top: e.target.scrollTop,
		});
		if(e.target.scrollTop <= 30){
			this.style.position = 'static';
			$('thead.scrollable').css('position', 'static');
		}
		/*if(e.target.scrollTop >= 30){
			this.style.position = 'relative';
			$('thead.scrollable').css({'position': 'absolute'}).offset({
				top: e.target.scrollTop + 10,
			});
		}  else{
			this.style.position = 'static';
			$('thead.scrollable').css('position', 'static');
		}*/
	});
	
// Расставить рейтинг в дата атрибуты

let $table = $('.table_container table tbody');
let rating = [];

$table.find('tr').each(function(ind, elem){
	// Depricated, перед продакшеном удалить
	let curTR = $(this);
	curTR.attr({
		'data-buy': Math.floor(Math.random() * 5) + 1,
		'data-bad': Math.floor(Math.random() * 3) + 1,
		'data-sell': Math.floor(Math.random() * 10) + 1,
		'data-date': (Math.floor(Math.random() * 30) + 1) + '.' + (Math.floor(Math.random() * 11) + 1) + '.2017',
		'data-id' : ind
	});
	// ^ Delete before production

	rating.push({
		'buy' : curTR.attr('data-buy'),
		'bad' : curTR.attr('data-bad'),
		'sell' : curTR.attr('data-sell'),
		'date' : curTR.attr('data-date'),
		'id' : curTR.attr('data-id')
	});

});

/* Показ жалоб */

let $showAbuse = $('.modal__content footer .show_abuse');
let $textAbuse = $('.text-abuse');

	$showAbuse.on('click', function(){
		let flagNotShowedAbuse = $textAbuse.hasClass('d-n');

		if(flagNotShowedAbuse){
			$textAbuse.removeClass('d-n');
			$(this).text('Скрыть жалобы');
		} else{
			$textAbuse.addClass('d-n');
			$(this).text('Показать жалобы');
		}
	});


/* Конец показа жалоб */


// Tooltip

	$('[data-toggle="tooltip"]').tooltip();

// Модальные окна

let $modalContainer = $('section.modal-container');
let classForClose = 'd-n';
let activeContentClass = 'active';

let $iconCloser = $('section.modal-container i.fa.closer, section.modal-container');
let $modalRating = $('section.modal-container div.modal__content.rating');
let $modalRules = $('section.modal-container .modal__content.rules');
let $modalFilters = $('section.modal-container .modal__content.filters');
let $modalStat = $('section.modal-container .modal__content.new-category');
let $modalMoreInfo = $('section.modal-container .modal__content.table_more_info');
let $modalHistory = $('section.modal-container .modal__content.show_history_pay');
let $modalPayBalance = $('section.modal-container .modal__content.pay_balance');
let $modalContacts = $('section.modal-container .modal__content.more_contacts');
let $modalReport = $('section.modal-container .modal__content.report');
let $modalAddStore = $('section.modal-container .modal__content.store');
let $modalNewMessage = $('section.modal-container .modal__content.new-message');
let $modalChat = $('section.modal-container .modal__content.chat-message');
let $modalVote = $('section.modal-container .modal__content.vote');
/*
$modalRating - модальное окно для рейтинга
$modalRules - модальное окно для правил
*/
let $showRules = $('label[for="rules"]');
let $showRating = $('.table_output table .btn.btn-info.output_search__button-rating');
let $showFilters = $('button.all_filters');
let $showStat = $('button.show_stat');
let $showMoreInfo = $('button.more-info');
let $showHistory = $('button.show_history_button');
let $showPayBallance = $('button.pay_balance_btn, button.pay_balance_btn_mod');
let $showContacts = $('button.show_contacts');
let $showReport = $('button.report');
let $showAddStore = $('button.add_store');
let $showNewMessage = $('button.send_message');
let $showChat = $('button.answer, .tr_answer');
let $showVote = $('.d-slide_future');
/*
$showRules - кнопка для показа модального окна правил
$showRating - кнопки для модального окна рейтинга
*/
	$iconCloser.on('click', function(e){
		if ($(e.target).is('section.modal-container') || $(e.target).is($iconCloser)){
			$('section.modal-container .modal__content.active').removeClass(activeContentClass);
			$modalContainer.addClass(classForClose);
		}
	});

	$showRating.on('click', function(){
		let trID = $(this).parents('tr').attr('data-id');
		$modalRating.find('table td span').each(function(ind, elem){
			let curSpan = $(this);
			$('.modal__content h4 + span').html(' ' + rating[trID].date);
			if(curSpan.hasClass('check_info')){
				curSpan.html(rating[trID].buy);
			} else if(curSpan.hasClass('bad_info')){
				curSpan.html(rating[trID].bad);
			} else if(curSpan.hasClass('stars_info')){
				curSpan.html(rating[trID].sell);
			}
		});
		$modalRating.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showRules.on('click', function(){
		$modalRules.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showVote.on('click', function(){
		$modalVote.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});


	$showFilters.on('click', function(){
		$modalFilters.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showStat.on('click', function(){
		$modalStat.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showMoreInfo.on('click', function(){
		$modalMoreInfo.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showHistory.on('click', function(){
		$modalHistory.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showContacts.on('click', function(){
		$modalContacts.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showReport.on('click', function(){
		$modalReport.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showPayBallance.on('click', function(){
		$modalHistory.removeClass(activeContentClass);
		$modalPayBalance.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showAddStore.on('click', function(){
		$modalAddStore.addClass(activeContentClass);
		$modalContainer.removeClass(classForClose);
	});

	$showNewMessage.on('click', function(){
			$modalNewMessage.addClass(activeContentClass);
			$modalContainer.removeClass(classForClose);
	});

	$showChat.on('click', function(e){
		if( $(e.target).is('.table-general.message tr.tr_answer td') || $(e.target).is('button.answer') ){
			$modalChat.addClass(activeContentClass);
			$modalContainer.removeClass(classForClose);
		}
	});

// Работа с чекбоксами в форме выдачи поиска, открываем кнопку и получаем массив отмеченных инпутов в виде DOM объекта
let $checkboxesProd = $('.table_output tbody tr input[type="checkbox"]');
let buttonToCart = document.querySelector('button.add_to_cart');
let buttonClearCart = document.querySelector('button.clear_cart');
let buttonDeactiv = document.querySelector('button.deactivation');
let listChecked = [];
	$(buttonClearCart).on('click', function(){
		if(confirm('Вы уверены?') && listChecked.length == 0){
			$('.table_output tbody').hide();
		}
	});
	$checkboxesProd.on('change', function(e){
		//console.log(this.checked);
		if(this.checked){
			listChecked.push(e.target);
		} else{
			listChecked.splice(e.target, 1);
		}
		if (listChecked.length == 0){
			try{
				buttonToCart.disabled = true;
				buttonDeactiv.disabled = true;
			}	catch(e){
				console.log(e);
			}
			try{
				buttonClearCart.innerHTML = 'Очистить всё';
			}	catch(e){
				console.log(e);
			}
		} else {
			try{
				buttonToCart.disabled = false;
				buttonDeactiv.disabled = false;
			}	catch(e){
				console.log(e);
			}
			try{
				buttonClearCart.innerHTML = 'Удалить выбранное';
			}	catch(e){
				console.log(e);
			}
		}
		//console.log(listChecked);
		return listChecked;
	});

// Работа с отзывами
let $buttonAbuse = $('.report__buttonChoose_abuse');
let $buttonGood = $('.report__buttonChoose_good');

let $formAbuse = $('.report__form.abuse');
let $formGood = $('.report__form.good');


	$buttonAbuse.on('click', function(e){
		if(!$formGood.hasClass('d-n')){
			$formGood.addClass('d-n');
		}
		$formAbuse.removeClass('d-n');
	});

	$buttonGood.on('click', function(e){
		if(!$formAbuse.hasClass('d-n')){
			$formAbuse.addClass('d-n');
		}
		$formGood.removeClass('d-n');
	});
// Получаем нужные объекты и переменные
let $containersStars = $('.report__form .radio-group'); // Контейнеры со звездами
let selector = 'input[type=radio]'; // Селектор для самих инпутов, так быстрее
let $labels = $('.report__form .radio-group label'); //Обертки для звезд
let colorStars = 'stars-rate'; //Класс, который будет красить звезды
let $radios = $(selector); // Все радио-кнопки
// Проставим всем контейнерам со звездами их статус и значение
	$containersStars.each(function(i, el){
		$(this).attr({
			'data-filled': false, // не выбран еще
			'data-value': '', // индекс инпута, вообще лишнее, можно будет позже убрать, если не пригодится
		});
	});
// Всем лейблам сделаем событие для зажигания звезд
	$labels.on('mouseenter', function(){
	// Сначала пройдемся по всем звездам с ховером и удалим класс
		$(this).parents('.radio-group').find('label').each(function(){
			$(this).removeClass(colorStars);
		});
	// Звезде с ховером зададим класс
		$(this).addClass(colorStars);
	// Всем предыдущим тоже зададим класс
		$(this).prevAll().each(function(){
			$(this).addClass(colorStars);
		});
	});
// При покидании курсора из контейнера, а не из звезды
	$containersStars.on('mouseleave', function(){
	// Получим значение контейнера, было ли в нем что-то отмечено или нет
		let filled = $(this).attr('data-filled'); // заполнен контейнер или нет
		let $checked = $(this).find('input:checked'); // радио-кнопка, которая отмечена
		// Удалим классы у всех радио-кнопок, если отмеченных полей нет - звезды должны погаснуть!
		$(this).find('label').each(function(){
			$(this).removeClass(colorStars);
		});
		// Проверим, было ли поле отмечено, если да, то...
		if(filled == 'true'){
			// Начиная с отметки и перед ней ставим класс всем звездам, всем предыдущим до отмеченной
			$checked.parent('label').prevAll('label').each(function(i, el){
				$(this).addClass(colorStars);
			});
		}
	});

// При изменении значения селектора перезаписывается значения у родителя
	
	$radios.on('change', function(){
		let $radio = $(this); // так короче и понятнее будет
		let $parent = $radio.parents('.radio-group'); // самый главный родитель-контейнер
		let parentStatus = $parent.attr('data-filled'); // статус родителя
		let parentValue = $parent.attr('data-value'); // значение родителя
		let value = $radio.val(); // записываем значение радио-кнопки в контейнер-родитель
		if(this.checked){
			// если текущая радио-кнопка отмечена, меняем статус родителя в true 
			parentStatus = true;
			$parent.attr('data-filled', parentStatus);
			$parent.attr('data-value', value);
		}
	});
//Для следующего кода

//Работа с вкладками настроек

let $spoilers = $('.settings section.section_settings h2');
let $formToHide = $('.settings section.section_settings:not(.always_opened) form');

	//Скроем настройки
	$formToHide.hide();

	//Повесим открытие форм на заголовки
	$spoilers.on('click', function(){
		$(this).next('form').slideToggle(500, $.bez([.23,.86,.68,1.08]));
		$(this).find('i.fa').toggleClass('rotation');
	});

//Найдем кнопки для добавления полей
let $addPhone = $('button.add_number');
let $addEmail = $('button.add_email');

// Найдем в контейнеру место, куда добавлять
let $insertPhone = $('section.section_settings.set_numbers hr.hr_phone_end');
let $insertEmail = $('section.section_settings.set_numbers hr.hr_email_end');

//Счетчик полей
let phoneIndex = 0;
let emailIndex = 0;
let max_count = 2;

	//Функция добавления телефона
	$addPhone.on('click', function(e){
		e.preventDefault();
		if(phoneIndex <= max_count){
			let $divForms = $('<div>').addClass('forms');
			let $label = $('<label>').addClass('form-label');
			let $inputPhone = $('<input>').attr('type', 'text').addClass('form-control');
			let $buttonDel = $('<button>').attr('type', 'button')
				.addClass('btn btn-danger delete')
				.html('<i class="fa fa-minus"></i> Удалить');
			phoneIndex++;

			if (phoneIndex > max_count){
				this.disabled = true;
			}

			$divForms.append($label.attr('for', 'number' + phoneIndex).text('Номер ' + phoneIndex + ':'));
			$divForms.append($inputPhone.attr('id', 'number' + phoneIndex));
			$divForms.append($buttonDel);
			$insertPhone.before($divForms);
		}
	});
	// Функция добавления почты
	$addEmail.on('click', function(e){
		e.preventDefault();
		if (emailIndex <= max_count){
			let $divForms = $('<div>').addClass('forms');
			let $label = $('<label>').addClass('form-label');
			let $inputEmail = $('<input>').attr('type', 'email').addClass('form-control');
			emailIndex++;

			if (emailIndex > max_count){
				this.disabled = true;
			}

			let $buttonDel = $('<button>').attr('type', 'button')
					.addClass('btn btn-danger delete')
					.html('<i class="fa fa-minus"></i> Удалить');
			$divForms.append($label.attr('for', 'email' + emailIndex).text('Почта ' + emailIndex + ':'));
			$divForms.append($inputEmail.attr('id', 'email' + emailIndex));
			$divForms.append($buttonDel);
			$insertEmail.before($divForms);
		}
	});

// Ширина пираммиды

let $statusArrayElements = $('section.info_section.your_status p');
let lengthArrayElements = $statusArrayElements.length;

	$statusArrayElements.each(function(i){
		let delta = 50 / lengthArrayElements;
		this.style.width = 50 + (delta * i) + '%';
		if(i == lengthArrayElements - 1){
			this.style.width = '100%';
		}
	});

// Админка
// Шаблоны
$('.new-template-create').on('click', function(){
	$(this).parents('main.main-content_list').addClass('d-n');
	$('main.main-content_redaction').removeClass('d-n');
});
$('.main-content_redaction a.main-content__link_back').on('click', function(e){
	e.preventDefault();
	$(this).parents('main.main-content_redaction').addClass('d-n');
	$('main.main-content_list').removeClass('d-n');
});

$('.change_mod_template').on('click', function(){
	$('form.d-n').removeClass('d-n');
	$(this).parents('form').addClass('d-n');
});

/* Tree  */

/*$('.tree__body').hide();*/

$('.tree__control_spoiler').on('click', function(e){
	if( $(e.target).hasClass('fa-plus') && $(e.target).is('i.fa') ){
		$(e.target).removeClass('fa-plus').addClass('fa-minus');
	} else if( $(e.target).hasClass('fa-minus') && $(e.target).is('i.fa') ) {
		$(e.target).removeClass('fa-minus').addClass('fa-plus');
	}
	$(e.target).parents('.tree__person').first().find('.tree__body').first().slideToggle();
});

/* End Tree */

/* Working with modal  */

let $actionButton = $('.show__modal');

	$actionButton.on('click', function(e){

		let modalName = $(this).attr('data-modal-name');
		let modalNameSelector = '.' + modalName;
		let modalTarget = $(this).attr('data-modal-target') || undefined;
		let modalTargetSelector = '.' + modalTarget;

		/*console.log(`
				modalName: ${modalName}
				modalTarget: ${modalTarget}
				e.target: ${[e.target]}
			`);*/
		
		
		$('.modal__content').removeClass(activeContentClass);
		
		$(modalNameSelector).addClass(activeContentClass);
		
		$modalContainer.removeClass(classForClose);
		
		if( modalTarget != undefined && $(modalNameSelector).has(modalTargetSelector) ){

			$(modalNameSelector + '> *:not(header)').css('display', 'none');

			$(modalTargetSelector).css('display', 'block');
		}
	});

// Managment links

$('.managment__button').on('click', e => {
	$('.managment__button').removeClass('managment__button_active');
	$(e.target).addClass('managment__button_active');
});

$('i.fa.tree__icon_ban').on('click', () => {
	$('.managment__button').removeClass('managment__button_active');
	$('.modal__content header.managment__header button[data-modal-target=managment_ban]').addClass('managment__button_active');
});

/* END working with modal */

/* Checkboxes for selection items and buttons */

let $checkboxes = $('.checkbox__manage');
let $buttonsBinded = $('.checkbox__bind_button');
let arrayCheckedItems = [];

if(arrayCheckedItems.length == 0){
	$buttonsBinded.prop('disabled', true);
}

	$checkboxes.on('change', function(){
		if(this.checked){
			arrayCheckedItems.push(this);
		} else if(!this.checked){
			arrayCheckedItems.splice(arrayCheckedItems.indexOf(this), 1);
		}

		if(arrayCheckedItems.length > 0){
			$buttonsBinded.prop('disabled', false);
		} else if(arrayCheckedItems.length == 0){
			$buttonsBinded.prop('disabled', true);
		}
	});


/* END Checkboxes */

/* Slide DIV */

let $slideActivator = $('.slide-div__activator');
let slideTargetClass = '.slide-div__target';
let $slideTarget = $(slideTargetClass);

	$slideTarget.hide();
	$slideActivator.on('click', function(){
		$(this).next(slideTargetClass).slideToggle();
	});

/* END Slide DIV */

/* Activation div system */
// Shows divs with display:none

let $activator = $('.activator-item');

	$activator.on('click', function(e){
		let activateTargetSelector = $(e.target).attr('data-activator-target'); //Needed a selecor
		$(activateTargetSelector).removeClass('d-n');
	});

/* END Activation div system */

// Table columns hide

	$('.table_output table tr').each(function(ind, elem){
		let tds = $(elem).find('td');
		tds.each(function(ind, elem){
			let current = $(elem);
			current.addClass('td_' + ind);
			current.attr('data-column', ind);
		});
	});
	$('.hide-column').on('click', function(e){
		let target = $(e.target);
		let columnID = '.td_' + target.parents('td').attr('data-column');
		console.log(columnID);
		$(columnID).toggleClass('hide_me');
	});
});